using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Http = System.Net.WebRequestMethods.Http;

namespace test_mvc.Controllers
{
    public class ProxyHandler : DelegatingHandler
    {
        private static HttpClient client = new HttpClient();

        protected async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request)
        {
            // strip the /proxy portion of the path when making the request
            // to the backend node because our server will be made to listen
            // to :80/proxy/*   (see below when registering the /proxy route)
            var forwardUri = new UriBuilder(request.RequestUri.AbsoluteUri.Replace("/proxy", string.Empty));

            // replace the port from 80 to the backend target port
            request.RequestUri = forwardUri.Uri;

            if (request.Method == HttpMethod.Get)
            {
                request.Content = null;
            }

            // replace the Host header when making the request to the 
            // backend node
            request.Headers.Host = "https://pcrms.pestream.com";
            var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
            return response;
        }
    }
}
