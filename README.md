This is a test application to test the bundling of an external vuejs app and including it in a asp.net mvc project.

After bundling the vuejs app with `npm run build` you will have a dist directory with a css folder, js folder, and an index.html page.

1. Copy to contents on the css folder to [wwwroot/css](https://gitlab.com/paultucker04/test-mvc-app/-/tree/master/test-mvc/wwwroot/css)
2. Copy to contents on the js folder to [wwwroot/js](https://gitlab.com/paultucker04/test-mvc-app/-/tree/master/test-mvc/wwwroot/js)
3. Copy then index.html into one the the views in the mvc app. I have been using the [Privacy Page](https://gitlab.com/paultucker04/test-mvc-app/-/blob/master/test-mvc/Views/Home/Privacy.cshtml) to test things out.
4. You must escape all `@` by adding another `@` before them resulting it `@@`. This is because `@` is a keyword in cshtml.
5. You must also prepend ~ in from of all paths to asset files. This tells the app to look in the wwwroot directory.